#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate regex;
extern crate rocket;
extern crate serde_json;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate rocket_contrib;

use regex::Regex;
use rocket::http::RawStr;
use rocket::request::LenientForm;
use rocket_contrib::{Json, Value};

/// # Slack outgoing webhook
///
/// [Slack docs](https://api.slack.com/custom-integrations/outgoing-webhooks)
/// ```
/// token=XXXXXXXXXXXXXXXXXX
/// team_id=T0001
/// team_domain=example
/// channel_id=C2147483705
/// channel_name=test
/// timestamp=1355517523.000005
/// user_id=U2147483697
/// user_name=Steve
/// text=googlebot: What is the air-speed velocity of an unladen swallow?
/// trigger_word=googlebot:
/// ```
#[allow(dead_code)]
#[derive(FromForm)]
struct SlackOutgoing {
    user_id: String,
    text: String,
}

#[post("/foglink/<name>", data = "<message_form>")]
fn foglink(name: &RawStr, message_form: LenientForm<SlackOutgoing>) -> Option<Json<Value>> {
    let message = message_form.into_inner();
    lazy_static! {
        static ref CASES: Regex = Regex::new(r"(?i)(?:case\s*|#)(\d+)").unwrap();
    }
    if message.user_id != "USLACKBOT" {
        let links: String = CASES.captures_iter(message.text.as_str())
            .map(|caps| {
                format!("<https://{name}.fogbugz.com/f/cases/{case}|Case {case}>",
                        name = name,
                        case = &caps[1])
            })
            .collect::<Vec<_>>()
            .join(", ");
        let response = json!({
            "text": links
        });
        Some(Json(response))
    } else {
        None
    }
}

#[post("/gitlablink/<host>/<group>/<project>", data = "<message_form>")]
fn gitlablink(host: &RawStr,
              group: &RawStr,
              project: &RawStr,
              message_form: LenientForm<SlackOutgoing>)
              -> Option<Json<Value>> {
    let message = message_form.into_inner();
    lazy_static! {
        static ref MERGE_REQUEST: Regex = Regex::new(r"(?i)(?:mr\s*|!)(\d+)").unwrap();
        static ref ISSUE: Regex = Regex::new(r"(?i)(?:issue\s*)(\d+)").unwrap();
    }
    if message.user_id != "USLACKBOT" {
        let merge_requests = MERGE_REQUEST.captures_iter(message.text.as_str())
            .map(|caps| {
                format!("<https://{host}/{group}/{project}/merge_requests/{mr}|Merge Request {mr}>",
                        host = host,
                        group = group,
                        project = project,
                        mr = &caps[1])
            });
        let issues = ISSUE.captures_iter(message.text.as_str()).map(|caps| {
            format!("<https://{host}/{group}/{project}/issues/{issue}|Issue {issue}>",
                    host = host,
                    group = group,
                    project = project,
                    issue = &caps[1])
        });
        let links: String = merge_requests.chain(issues)
            .collect::<Vec<_>>()
            .join(", ");
        let response = json!({
            "text": links
        });
        Some(Json(response))
    } else {
        None
    }
}

fn main() {
    rocket::ignite().mount("/", routes![foglink, gitlablink]).launch();
}
